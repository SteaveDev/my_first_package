import * as crypto from "crypto";

export function concatener(number1: string, number2: string): string {
    return number1 + number2;
}

export function aleatoire(): string {
    return crypto.randomUUID({"disableEntropyCache": true});
}