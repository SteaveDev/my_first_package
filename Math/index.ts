export function addition(number1: number, number2: number): number {
    return number1 + number2;
}

export function soustraction(number1: number, number2: number): number {
    return number1 - number2;
}

export function multiplication(number1: number, number2: number): number {
    return number1 * number2;
}

export function division(number1: number, number2: number): number {
    if(number1 == 0 || number2 ==0)
        return -1;
    return number1 / number2;
}
